//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use vergen::{vergen, Config};

fn main() -> anyhow::Result<()> {
    inspect();
    vergen(Config::default())
}

#[rustversion::nightly]
fn inspect() {
    println!("cargo:rustc-cfg=result_option_inspect");
}

#[rustversion::not(nightly)]
fn inspect() {}
