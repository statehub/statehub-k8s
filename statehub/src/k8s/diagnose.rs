//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

// use std::fmt;

use async_stream::try_stream;
use console::Emoji;
use futures::stream::Stream;

use statehub_cluster_operator::cluster::CheckObject;

use super::*;

const CHECK: Emoji<'static, 'static> = Emoji("\u{2705}", "√");
const QUESTION: Emoji<'static, 'static> = Emoji("\u{2753}", "?");
const CROSS: Emoji<'static, 'static> = Emoji("\u{274c}", "x");

#[derive(Debug)]
pub(crate) enum Probe {
    Good(String),
    Partial(String),
    Bad(String),
}

impl Probe {
    pub(crate) fn report(&self, label: &str) -> String {
        let (mark, text) = match self {
            Probe::Good(text) => (CHECK, text),
            Probe::Partial(text) => (QUESTION, text),
            Probe::Bad(text) => (CROSS, text),
        };
        format!("{:<60} - {} ({})", label, mark, text)
    }

    pub(crate) fn good(text: impl ToString) -> Self {
        let text = text.to_string();
        Self::Good(text)
    }

    pub(crate) fn partial(text: impl ToString) -> Self {
        let text = text.to_string();
        Self::Partial(text)
    }

    pub(crate) fn bad(text: impl ToString) -> Self {
        let text = text.to_string();
        Self::Bad(text)
    }
}

// trait Diagnose {
//     fn diagnose(&self, text: &str) -> Result<String, String>;
// }

// impl<T: fmt::Display> Diagnose for anyhow::Result<T> {
//     fn diagnose(&self, label: &str) -> Result<String, String> {
//         self.as_ref()
//             .map(|text| format!("{:<60} - {} ({})", label, CHECK, text))
//             .map_err(|err| format!("{:<60} - {} ({})", label, CROSS, err))
//     }
// }

// impl<T: fmt::Display> Diagnose for kube::Result<T> {
//     fn diagnose(&self, label: &str) -> Result<String, String> {
//         self.as_ref()
//             .map(|text| format!("{:<60} - {} ({})", label, CHECK, text))
//             .map_err(|err| format!("{:<60} - {} ({})", label, CROSS, err))
//     }
// }

impl Kubectl {
    pub(crate) async fn check_cluster(&self) -> impl Stream<Item = anyhow::Result<String>> + '_ {
        try_stream!(
            let config = self.get_config().await?;
            let operator = statehub_cluster_operator::cluster::Operator::with_config(config)?;

            let crd = self
                .check_statehub_cluster_crd()
                .await?
                .report("StatehubCluster CRD is installed correctly");
            yield crd;

            let clusters = operator.get_clusters().await?;

            let (probe, cluster) = self
                .validate_statehub_cluster_object(clusters)
                .await;
            yield probe.report("StatehubCluster Object is present");

            if let Some(cluster) = cluster {
                let namespace = operator
                    .check_namespace(&cluster)
                    .await?;
                let probe = validate_namespace(&cluster, namespace);
                yield probe.report("Namespace exists");

                let probe = operator
                    .check_configmap(&cluster)
                    .await
                    .map(validate_configmap)?;
                yield probe.report("ConfigMap exists");

                let probe = operator
                    .check_cluster_token_secret(&cluster)
                    .await
                    .map(validate_cluster_token_secret)?;
                yield probe.report("Cluster Token secret exists");

                let probe = operator
                    .check_aws_credentials(&cluster)
                    .await
                    .map(validate_cloud_credentials)?;
                yield probe.report("AWS Credentials secret exists");

                let probe = operator
                    .check_azure_credentials(&cluster)
                    .await
                    .map(validate_cloud_credentials)?;
                yield probe.report("Azure Credentials secret exists");

                let probe = operator
                    .check_state_controller(&cluster)
                    .await
                    .map(validate_deployment)?;
                yield probe.report("state-controller exists");

                let probe = operator
                    .check_statehub_controller(&cluster)
                    .await
                    .map(validate_deployment)?;
                yield probe.report("statehub-controller exists");

                let probe = operator
                    .check_statehub_csi_controller(&cluster)
                    .await
                    .map(validate_deployment)?;
                yield probe.report("statehub-csi-controller exists");

                let probe = operator
                    .check_statehub_csi_node(&cluster)
                    .await
                    .map(validate_daemonset)?;
                yield probe.report("statehub-csi-node exists");
            }
        )
    }
}

fn validate_namespace(cluster: &StatehubCluster, namespace: Option<corev1::Namespace>) -> Probe {
    if let Some(namespace) = namespace {
        Probe::good(format!(
            "{} ({})",
            namespace.name(),
            namespace.finalizers().join(",")
        ))
    } else {
        Probe::bad(format!("'{}' namespace not found", cluster.namespace()))
    }
}

fn validate_configmap(configmap: CheckObject<corev1::ConfigMap>) -> Probe {
    if configmap.found() {
        Probe::good(configmap.name())
    } else {
        Probe::bad(format!("ConfigMap '{}' not found", configmap.name()))
    }
}

fn validate_cluster_token_secret(secret: CheckObject<corev1::Secret>) -> Probe {
    if secret.found() {
        Probe::good(secret.name())
    } else {
        Probe::bad(format!("Secret '{}' not found", secret.name()))
    }
}

fn validate_cloud_credentials(secret: CheckObject<corev1::Secret>) -> Probe {
    if secret.found() {
        Probe::good(secret.name())
    } else {
        Probe::bad(format!("Secret '{}' not found", secret.name()))
    }
}

fn validate_deployment(deployment: CheckObject<appsv1::Deployment>) -> Probe {
    if deployment.found() {
        Probe::good(deployment.name())
    } else {
        Probe::bad(format!("Deployment '{}' not found", deployment.name()))
    }
}

fn validate_daemonset(daemonset: CheckObject<appsv1::DaemonSet>) -> Probe {
    if daemonset.found() {
        Probe::good(daemonset.name())
    } else {
        Probe::bad(format!("DaemonSet '{}' not found", daemonset.name()))
    }
}
