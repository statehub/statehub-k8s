//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

const K8S_TOPOLOGY_REGION: &str = "topology.kubernetes.io/region";
const K8S_TOPOLOGY_ZONE: &str = "topology.kubernetes.io/zone";
const AKS_CLUSTER: &str = "kubernetes.azure.com/cluster";

trait LabelsExt: ResourceExt {
    fn label(&self, label: impl AsRef<str>) -> Option<&str> {
        self.labels().get(label.as_ref()).map(String::as_str)
    }

    fn region(&self) -> Option<&str> {
        self.label(K8S_TOPOLOGY_REGION)
    }

    fn zone(&self) -> Option<&str> {
        self.label(K8S_TOPOLOGY_ZONE)
    }
}

impl<R> LabelsExt for R where R: ResourceExt {}

pub(super) fn group_nodes_by_region(
    nodes: impl IntoIterator<Item = corev1::Node>,
) -> HashMap<Option<String>, Vec<String>> {
    nodes
        .into_iter()
        .map(|node| (node.name(), node.region().map(ToString::to_string)))
        .fold(HashMap::new(), |mut regions, (name, region)| {
            regions.entry(region).or_default().push(name);
            regions
        })
}

pub(super) fn group_nodes_by_zone(
    nodes: impl IntoIterator<Item = corev1::Node>,
) -> HashMap<Option<String>, Vec<String>> {
    nodes
        .into_iter()
        .map(|node| (node.name(), node.zone().map(ToString::to_string)))
        .fold(HashMap::new(), |mut zones, (name, zone)| {
            zones.entry(zone).or_default().push(name);
            zones
        })
}

pub(super) fn is_aks<'a>(nodes: impl IntoIterator<Item = &'a corev1::Node>) -> bool {
    nodes
        .into_iter()
        .any(|node| node.label(AKS_CLUSTER).is_some())
}

pub(super) fn is_eks<'a>(nodes: impl IntoIterator<Item = &'a corev1::Node>) -> bool {
    nodes.into_iter().any(|node| {
        node.status
            .as_ref()
            .and_then(|status| status.node_info.as_ref())
            .map(|info| info.kubelet_version.contains("eks"))
            .unwrap_or_default()
    })
}

pub(super) enum NamesAndContexts {
    GuessEverything,
    ClusterWithCurrentContext(v0::ClusterName),
    NamedContext(String),
    ExactPairs(Vec<(v0::ClusterName, String)>),
}

impl TryFrom<(Vec<v0::ClusterName>, Vec<String>)> for NamesAndContexts {
    type Error = anyhow::Error;

    fn try_from(value: (Vec<v0::ClusterName>, Vec<String>)) -> Result<Self, Self::Error> {
        let (mut names, mut contexts) = value;

        if names.is_empty() && contexts.is_empty() {
            Ok(Self::GuessEverything)
        } else if names.len() == 1 && contexts.is_empty() {
            let name = names.remove(0);
            Ok(Self::ClusterWithCurrentContext(name))
        } else if names.is_empty() && contexts.len() == 1 {
            let context = contexts.remove(0);
            Ok(Self::NamedContext(context))
        } else if names.len() == contexts.len() {
            let pairs = names.into_iter().zip(contexts.into_iter()).collect();
            Ok(Self::ExactPairs(pairs))
        } else {
            let names = names.len();
            let contexts = contexts.len();
            Err(anyhow::anyhow!(
                "Multiple clusters ({}) need adequate amount of contexts ({} given)",
                names,
                contexts
            ))
        }
    }
}
