//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

impl Kubectl {
    pub(super) async fn set_secret(
        &self,
        namespace: &str,
        r#type: &str,
        name: &str,
        string_data: json::Value,
    ) -> anyhow::Result<corev1::Secret> {
        let secrets = self.namespaced_api::<corev1::Secret>(namespace).await?;
        let secret = secrets.get(name).await;

        let (secret, replace) = if let Ok(secret) = secret {
            let secret = json::from_value(json::json!({
                "apiVerion": "v1",
                "kind": "Secret",
                "metadata": {
                    "name": name,
                    "resourceVersion": secret.resource_version(),
                },
                "type": r#type,
                "stringData": string_data,
            }))?;

            (secret, true)
        } else {
            let secret = json::from_value(json::json!({
                "apiVerion": "v1",
                "kind": "Secret",
                "metadata": {
                    "name": name,
                },
                "type": r#type,
                "stringData": string_data,
            }))?;

            (secret, false)
        };

        let pp = self.post_params();

        let secret = if replace {
            secrets.replace(name, &pp, &secret).await?
        } else {
            secrets.create(&pp, &secret).await?
        };
        Ok(secret)
    }
}
