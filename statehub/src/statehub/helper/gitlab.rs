//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use crate::api::GitlabGenericPackage;

use super::*;

impl StateHub {
    pub(super) async fn latest_version(&self) -> anyhow::Result<semver::Version> {
        let bytes = api::Api::gitlab()
            .get_file(Self::PLATFORM, "latest", "version")
            .await?;
        String::from_utf8_lossy(&bytes)
            .trim()
            .parse()
            .context("Reading latest/version")
    }

    pub(super) async fn get_package(
        &self,
        package: &str,
        version: &str,
    ) -> anyhow::Result<bytes::Bytes> {
        api::Api::gitlab()
            .get_file(Self::PLATFORM, version, package)
            .await
    }
}
