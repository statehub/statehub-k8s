//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::env;
use std::fs;
use std::path::PathBuf;

use anyhow::Context;
use chrono::Duration;
use console::Style;
use serde::{Deserialize, Serialize};

use self::v1::Config as ConfigV1;
use self::v2::Config as ConfigV2;
use self::v3::Config as ConfigV3;

mod v1;
mod v2;
mod v3;

const STATEHUB_HOME: &str = "STATEHUB_HOME";

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub(crate) struct Config {
    profile: Option<String>,
    config: ConfigV3,
}

const CONFIG_FILE: &str = "config.toml";
const UPDATE_FILE: &str = ".last_check";
const DEFAULT_API: &str = "https://api.statehub.io";
const DEFAULT_CONSOLE: &str = "https://console.statehub.io";

impl Config {
    pub(crate) fn api(&self) -> &str {
        self.config
            .api(self.profile.as_deref())
            .trim()
            .trim_end_matches('/')
    }

    pub(crate) fn console(&self) -> &str {
        self.config
            .console(self.profile.as_deref())
            .trim()
            .trim_end_matches('/')
    }

    pub(crate) fn token(&self) -> Option<&str> {
        self.config.token(self.profile.as_deref())
    }

    pub(crate) fn style(&self) -> Style {
        self.config
            .color(self.profile.as_deref())
            .map(Style::from_dotted_str)
            .unwrap_or_default()
    }

    pub(crate) fn optionally_management_api(self, api: Option<String>) -> Self {
        let Self {
            profile,
            mut config,
        } = self;

        if let Some(api) = api {
            config.set_api(profile.as_deref(), api);
        }

        Self { profile, config }
    }

    pub(crate) fn optionally_management_console(self, console: Option<String>) -> Self {
        let Self {
            profile,
            mut config,
        } = self;

        if let Some(console) = console {
            config.set_console(profile.as_deref(), console);
        }

        Self { profile, config }
    }

    pub(crate) fn set_token(self, token: Option<String>) -> Self {
        let Self {
            profile,
            mut config,
        } = self;

        if let Some(token) = token {
            config.set_token(profile.as_deref(), token);
        }

        Self { profile, config }
    }

    pub(crate) fn load(profile: Option<String>) -> anyhow::Result<Self> {
        let path = Self::config_file()?;
        anyhow::ensure!(path.exists(), "Config file does not exist");
        let config = fs::read_to_string(path)
            .context("Reading config file")
            .and_then(|text| toml::from_str::<toml::Value>(&text).context("Parsing config file"))?;

        let config =
            ConfigV3::validate_config(&config).unwrap_or_else(|_| Self::rolling_load(&config));

        Ok(Self { profile, config })
    }

    fn rolling_load(config: &toml::Value) -> ConfigV3 {
        if let Ok(v2) = ConfigV2::validate_config(config) {
            ConfigV3::from(v2)
        } else if let Ok(v1) = ConfigV1::validate_config(config) {
            let v2 = ConfigV2::from(v1);
            ConfigV3::from(v2)
        } else {
            ConfigV3::default()
        }
    }

    pub(crate) fn save(&self, set_default_profile: bool) -> anyhow::Result<PathBuf> {
        let dir = Self::statehub_home()?;
        fs::create_dir_all(dir).context("Creating config filesystem hierarchy")?;
        let path = Self::config_file()?;

        let mut config = self.config.clone();
        if set_default_profile {
            if let Some(ref profile) = self.profile {
                config.set_profile(profile);
            }
        }

        let contents = toml::to_string_pretty(&config).context("Serializing config")?;
        fs::write(&path, contents)
            .context("Writing config")
            .map(|_| path)
    }

    pub(crate) fn touch_update() {
        if let Ok(path) = Self::update_file() {
            fs::OpenOptions::new()
                .write(true)
                .truncate(true)
                .create(true)
                .open(path)
                .ok();
        }
    }

    fn check_update() -> Option<Duration> {
        let path = Self::update_file().ok()?;
        let std = fs::metadata(path).ok()?.modified().ok()?.elapsed().ok()?;
        Duration::from_std(std).ok()
    }

    fn statehub_home() -> anyhow::Result<PathBuf> {
        let env = env::var(STATEHUB_HOME).ok().map(PathBuf::from);
        let default = directories::UserDirs::new()
            .map(|user| user.home_dir().to_path_buf().join(".statehub"));

        env.or(default).context("Config directory name")
    }

    fn config_file() -> anyhow::Result<PathBuf> {
        Self::statehub_home().map(|home| home.join(CONFIG_FILE))
    }

    fn update_file() -> anyhow::Result<PathBuf> {
        Self::statehub_home().map(|home| home.join(UPDATE_FILE))
    }

    pub(crate) fn more_than_one_day_passed_since_last_check() -> bool {
        let last_check = Self::check_update().unwrap_or_else(Duration::max_value);
        last_check > Duration::days(1)
    }
}
