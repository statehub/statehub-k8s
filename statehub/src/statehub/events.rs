//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::process::Command;
use std::time;

use serde::ser;

use super::*;

pub(super) trait Event<T, U>: Sized {
    fn update(self, result: Result<&T, &U>) -> Self;
}

pub(super) trait WithEvent<E, T, U> {
    fn with_event(self, event: E) -> EventResult<E, T, U>;
}

pub(super) struct EventResult<E, T, U> {
    event: E,
    result: Result<T, U>,
}

impl<E, T, U> WithEvent<E, T, U> for Result<T, U> {
    fn with_event(self, event: E) -> EventResult<E, T, U> {
        EventResult {
            event,
            result: self,
        }
    }
}

impl StateHub {
    pub(super) async fn report_event<E, T, U>(&self, event: EventResult<E, T, U>) -> Result<T, U>
    where
        E: serde::Serialize + Event<T, U>,
    {
        let EventResult { event, result } = event;
        let event = event.update(result.as_ref());
        let reporter = env!("CARGO_PKG_NAME");
        let version = env!("CARGO_PKG_VERSION");
        let report = v0::Report::new(reporter, version, event);
        let _ = self.api.report_event(&report).await;
        result
    }

    pub(super) fn install_statehub_cluster_event(&self, namespace: &str) -> StatehubEvent {
        StatehubEvent::new(namespace, "StatehubCluster object installation")
    }

    pub(super) fn uninstall_statehub_cluster_event(&self, namespace: &str) -> StatehubEvent {
        StatehubEvent::new(namespace, "StatehubCluster object uninstallation")
    }

    pub(super) fn store_cluster_token_event(&self, namespace: &str) -> StatehubEvent {
        StatehubEvent::new(namespace, "Cluster Token Secret setup")
    }

    pub(super) fn setup_configmap_event(&self, namespace: &str) -> StatehubEvent {
        StatehubEvent::new(namespace, "ConfigMap setup")
    }

    pub(super) fn helm_install_event(
        &self,
        namespace: &str,
        commands: &[Command],
    ) -> StatehubHelmEvent {
        StatehubHelmEvent::new(namespace, commands)
    }
}

#[derive(Debug, Serialize)]
pub(super) struct StatehubEvent {
    #[serde(serialize_with = "serialize_duration")]
    duration: time::Instant,
    namespace: String,
    message: String,
}

impl StatehubEvent {
    fn new(namespace: &str, message: impl ToString) -> Self {
        let duration = time::Instant::now();
        let namespace = namespace.to_string();
        let message = message.to_string();
        Self {
            duration,
            namespace,
            message,
        }
    }
}

impl<T, U> Event<T, U> for StatehubEvent
where
    U: std::fmt::Display,
{
    fn update(self, result: Result<&T, &U>) -> Self {
        let message = match result {
            Ok(_) => format!("{} succeeded", self.message),
            Err(err) => format!("{} failed: {}", self.message, err),
        };

        Self { message, ..self }
    }
}

#[derive(Debug, Serialize)]
pub(super) struct StatehubHelmEvent {
    #[serde(serialize_with = "serialize_duration")]
    duration: time::Instant,
    namespace: String,
    commands: Vec<String>,
    stdout: String,
    stderr: String,
}

impl StatehubHelmEvent {
    pub(super) fn new(namespace: &str, commands: &[Command]) -> Self {
        let duration = time::Instant::now();
        let namespace = namespace.to_string();
        let commands = commands.iter().map(Show::show).collect::<Vec<_>>();
        let stdout = String::new();
        let stderr = String::new();
        Self {
            duration,
            namespace,
            commands,
            stdout,
            stderr,
        }
    }
}

impl<U: ToString> Event<(String, String), U> for StatehubHelmEvent {
    fn update(self, result: Result<&(String, String), &U>) -> Self {
        match result {
            Ok((stdout, stderr)) => {
                let stdout = stdout.to_string();
                let stderr = stderr.to_string();
                Self {
                    stdout,
                    stderr,
                    ..self
                }
            }
            Err(err) => {
                let stderr = err.to_string();
                Self { stderr, ..self }
            }
        }
    }
}

fn serialize_duration<S>(duration: &time::Instant, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    let duration = duration.elapsed();
    chrono::Duration::from_std(duration)
        .map_err(ser::Error::custom)?
        .to_string()
        .serialize(serializer)
}

#[cfg(test)]
mod tests;
