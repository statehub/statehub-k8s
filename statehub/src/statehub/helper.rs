//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::env;
use std::fs;

use anyhow::Context;
use indexmap::IndexMap;
use serde_json as json;
use tokio::time;

use crate::statehub::events::WithEvent;

use super::*;

mod gitlab;

#[derive(Clone, Debug)]
pub(super) struct RegisterCluster {
    helm: k8s::Helm,
    provider: Option<v0::Provider>,
    namespace: String,
    states: Option<Vec<v0::StateName>>,
    default_state: Option<String>,
    claim_unowned_states: bool,
    cleanup_grace: String,
    aws: AwsCredentials,
    azure: AzureCredentials,
}

impl RegisterCluster {
    #[allow(clippy::too_many_arguments)]
    pub(super) fn new(
        helm: k8s::Helm,
        provider: Option<v0::Provider>,
        namespace: String,
        states: Option<Vec<v0::StateName>>,
        default_state: Option<String>,
        claim_unowned_states: bool,
        cleanup_grace: String,
        aws: AwsCredentials,
        azure: AzureCredentials,
    ) -> Self {
        Self {
            helm,
            provider,
            namespace,
            states,
            default_state,
            claim_unowned_states,
            cleanup_grace,
            aws,
            azure,
        }
    }

    pub(super) fn states(&self) -> Option<&[v0::StateName]> {
        self.states.as_deref()
    }
}

impl StateHub {
    #[allow(clippy::too_many_arguments)]
    pub(super) async fn interactive_register_cluster_helper(
        &self,
        names: Vec<v0::ClusterName>,
        contexts: Vec<String>,
        states: Vec<v0::StateName>,
        no_state: bool,
        no_default_storage_class: bool,
        default_storage_class: Option<String>,
        no_state_owner: bool,
        namespace: String,
        skip_helm: bool,
        provider: Option<v0::Provider>,
        cleanup_grace: String,
        aws: AwsCredentials,
        azure: AzureCredentials,
    ) -> anyhow::Result<(Vec<k8s::Kubectl>, RegisterCluster)> {
        anyhow::ensure!(
            !(names.len() > 1 && contexts.len() > 1),
            "Multiple clusters and contexts specified - not supported for interactive register-cluster"
        );

        let kubectl = k8s::Kubectl::default()?;
        let all_contexts = kubectl.all_contexts();
        let default_context = contexts
            .into_iter()
            .next()
            .filter(|ctx| all_contexts.contains(&ctx.as_str()));

        let context = if let Some(context) = default_context {
            context
        } else {
            self.select_with_default(
                "Select KUBECONFIG context to register",
                all_contexts,
                kubectl.context(),
            )
            .unwrap_or_default()
        };

        let kubectl = kubectl.set_context(context);

        let default_name = names
            .into_iter()
            .next()
            .unwrap_or_else(|| kubectl.cluster_name().clone());

        let cluster_name = self.input_with_default("Choose K8s cluster name", default_name)?;

        let kubectl = kubectl.set_cluster_name(cluster_name);

        let namespace =
            self.input_with_default("Choose K8s Namespace to install statehub", namespace)?;

        let skip_helm = skip_helm
            || !self.confirm_with_default(
                "Run 'helm install' to install Statehub K8s components",
                true,
            );
        let helm = k8s::Helm::new(skip_helm);

        let all_states = self.api.get_all_states().await.unwrap_or_default();
        let unowned_states = all_states.iter().any(|state| state.owner.is_none());
        let all_states = all_states
            .into_iter()
            .map(|state| state.name)
            .collect::<Vec<_>>();

        let no_default_storage_class = if no_state {
            true
        } else {
            no_default_storage_class
        };

        let default_state = if no_default_storage_class {
            None
        } else if default_storage_class.is_none() {
            states.first().map(ToString::to_string)
        } else {
            default_storage_class
        };

        let states = if no_state || all_states.is_empty() {
            None
        } else if states.is_empty() {
            Some(states)
        } else {
            let default = all_states
                .iter()
                .position(|item| item.0 == "default")
                .unwrap_or_default();

            self.multiselect_with_default(
                "Select states to use with this cluster",
                &all_states,
                default,
            )
            .ok()
        };

        let claim_unowned_states = !no_state_owner
            && unowned_states
            && self.confirm_with_default("Claim ownership of unowned states", true);

        let locations = kubectl.collect_node_locations().await.unwrap_or_default();

        let aws = if locations.iter().any(|location| location.is_aws()) {
            self.aws_credentials_helper(aws)
        } else {
            aws
        };
        let azure = if locations.iter().any(|location| location.is_azure()) {
            self.azure_credentials_helper(azure)
        } else {
            azure
        };

        let register_cluster = RegisterCluster::new(
            helm,
            provider,
            namespace,
            states,
            default_state,
            claim_unowned_states,
            cleanup_grace,
            aws,
            azure,
        );

        Ok((vec![kubectl], register_cluster))
    }

    #[allow(clippy::too_many_arguments)]
    pub(super) async fn non_interactive_register_cluster_helper(
        &self,
        names: Vec<v0::ClusterName>,
        contexts: Vec<String>,
        states: Vec<v0::StateName>,
        no_state: bool,
        no_default_storage_class: bool,
        default_storage_class: Option<String>,
        no_state_owner: bool,
        namespace: String,
        skip_helm: bool,
        provider: Option<v0::Provider>,
        cleanup_grace: String,
        aws: AwsCredentials,
        azure: AzureCredentials,
    ) -> anyhow::Result<(Vec<k8s::Kubectl>, RegisterCluster)> {
        let kubectls = k8s::Kubectl::with_names_and_contexts(names, contexts)?;
        let no_default_storage_class = if no_state {
            true
        } else {
            no_default_storage_class
        };

        let default_state = if no_default_storage_class {
            None
        } else if default_storage_class.is_none() {
            states.first().map(ToString::to_string)
        } else {
            default_storage_class
        };

        let states = if no_state { None } else { Some(states) };
        let claim_unowned_states = !no_state_owner;

        let helm = k8s::Helm::new(skip_helm);

        let register_cluster = RegisterCluster::new(
            helm,
            provider,
            namespace,
            states,
            default_state,
            claim_unowned_states,
            cleanup_grace,
            aws,
            azure,
        );

        Ok((kubectls, register_cluster))
    }

    pub(super) async fn register_cluster_helper(
        &self,
        kubectl: k8s::Kubectl,
        locations: &[Location],
        register_cluster: &RegisterCluster,
    ) -> anyhow::Result<()> {
        let provider = match register_cluster.provider {
            Some(provider) => provider,
            None => kubectl.get_cluster_provider().await?,
        };

        let cluster_name = kubectl.cluster_name();
        let namespace = &register_cluster.namespace;

        kubectl.ensure_statehub_cluster_is_not_installed().await?;

        let cluster = self
            .api
            .register_cluster(cluster_name, namespace, provider, locations)
            .await
            .map(Quiet)?;

        self.inform(format_args!(
            "Registering {:#} cluster {} in {}",
            provider,
            cluster.name,
            locations.show(),
        ));

        let event = self.install_statehub_cluster_event(namespace);
        let event = kubectl
            .install_statehub_cluster(namespace)
            .await
            .with_event(event);
        let statehub_cluster = self.report_event(event).await?;

        self.setup_cluster_token_helper(&kubectl, &cluster, &statehub_cluster)
            .await?;
        self.setup_credentials_helper(
            &kubectl,
            &statehub_cluster,
            &register_cluster.aws,
            &register_cluster.azure,
        )
        .await?;
        self.setup_configmap_helper(
            &kubectl,
            &statehub_cluster,
            register_cluster.default_state.as_deref().unwrap_or(""),
            &register_cluster.cleanup_grace,
        )
        .await?;

        if register_cluster.claim_unowned_states {
            self.claim_unowned_states_helper(&cluster, register_cluster)
                .await?;
        }

        self.helm_install_helper(
            &kubectl,
            &cluster,
            &register_cluster.namespace,
            &register_cluster.helm,
        )
        .await?;

        cluster.print(&self.stdout, &self.style, self.json)
    }

    pub(super) async fn unregister_cluster_helper(
        &self,
        kubectl: k8s::Kubectl,
        helm: k8s::Helm,
    ) -> anyhow::Result<()> {
        let name = kubectl.cluster_name();
        let cluster = self.api.get_cluster(name).await?;

        if let Ok((statehub_cluster_name, namespace)) =
            kubectl.get_statehub_cluster_name_and_namespace().await
        {
            anyhow::ensure!(
                *name == statehub_cluster_name,
                "Cluster name mismatch: K8s thinks it is '{}', while Statehub thinks it is '{}'",
                statehub_cluster_name,
                name,
            );

            self.helm_uninstall_helper(&kubectl, &cluster, &namespace, &helm)
                .await?;

            let event = self.uninstall_statehub_cluster_event(&namespace);
            let event = kubectl.uninstall_statehub_cluster().await.with_event(event);
            self.report_event(event).await?;
        }

        self.relinquish_states_helper(name).await?;

        self.api
            .unregister_cluster(name)
            .await
            .print(&self.stdout, &self.style, self.json)?;

        Ok(())
    }

    fn aws_credentials_helper(&self, aws: AwsCredentials) -> AwsCredentials {
        if aws.missing() {
            self.inform("AWS/EKS cluster detected and you didn't provide any AWS credentials");
            self.inform(
                "Please follow this link to create AWS credentials: https://statehub.io/creds/aws",
            );
            self.inform("When done please copy/paste credentials here");
            let access_key_id = self.input("AWS_ACCESS_KEY_ID").unwrap_or_default();
            let secret_access_key = self.input("AWS_SECRET_ACCESS_KEY").unwrap_or_default();
            AwsCredentials::new(access_key_id, secret_access_key)
        } else {
            aws
        }
    }

    fn azure_credentials_helper(&self, azure: AzureCredentials) -> AzureCredentials {
        let azcli_installed = which::which("az").is_ok();
        if azure.missing() {
            self.inform("Azure/AKS cluster detected and you didn't provide any Azure credentials");
            if azcli_installed {
                self.inform("Since you seem to have AZ CLI installed please run following command");
            } else {
                self.inform("Please follow this link to open Azure Cloud Shell: https://shell.azure.com/bash");
                self.inform("and run following command there");
            }
            self.inform("wget https://get.statehub.io/create_statehub_service_principal.sh && bash create_statehub_service_principal.sh");
            self.inform("When done please copy/paste credentials here");
            let tenant_id = self.input("AZURE_TENANT_ID").unwrap_or_default();
            let client_id = self.input("AZURE_CLIENT_ID").unwrap_or_default();
            let client_secret = self.input("AZURE_CLIENT_SECRET").unwrap_or_default();
            AzureCredentials::new(tenant_id, client_id, client_secret)
        } else {
            azure
        }
    }

    pub(super) async fn add_location_helper(
        &self,
        state: &v0::State,
        location: &Location,
        wait: bool,
    ) -> anyhow::Result<()> {
        log::info!("Extending {} to {}", state, location);

        match location {
            Location::Aws(region) => {
                self.add_aws_location_helper(&state.name, *region, wait)
                    .await?;
            }
            Location::Azure(region) => {
                self.add_azure_location_helper(&state.name, *region, wait)
                    .await?;
            }
        }

        Ok(())
    }

    async fn add_aws_location_helper(
        &self,
        name: &v0::StateName,
        region: AwsRegion,
        wait: bool,
    ) -> anyhow::Result<Output<v0::StateLocationAws>> {
        let aws = self.api.add_aws_location(name, region).await?;
        if wait {
            let delay = time::Duration::from_secs(5);
            loop {
                if self
                    .api
                    .get_aws_location(name, region)
                    .await?
                    .status
                    .is_final()
                {
                    break;
                }
                time::sleep(delay).await;
            }
        }
        Ok(aws)
    }

    async fn add_azure_location_helper(
        &self,
        name: &v0::StateName,
        region: AzureRegion,
        wait: bool,
    ) -> anyhow::Result<Output<v0::StateLocationAzure>> {
        let azure = self.api.add_azure_location(name, region).await?;
        if wait {
            let delay = time::Duration::from_secs(5);
            loop {
                if self
                    .api
                    .get_azure_location(name, region)
                    .await?
                    .status
                    .is_final()
                {
                    break;
                }
                time::sleep(delay).await;
            }
        }
        Ok(azure)
    }

    pub(super) async fn remove_location_helper(
        &self,
        state: &v0::State,
        location: &Location,
    ) -> anyhow::Result<()> {
        log::info!("Truncating {} from {}", state, location);

        let name = state.name.clone();

        match location {
            Location::Aws(region) => self
                .api
                .del_aws_location(name, *region)
                .await
                .map(|_aws| ()),
            Location::Azure(region) => self
                .api
                .del_azure_location(name, *region)
                .await
                .map(|_azure| ()),
        }
    }

    pub(super) async fn adjust_all_states(
        &self,
        states: &[v0::StateName],
        locations: &[Location],
        wait: bool,
    ) -> anyhow::Result<()> {
        let missing_locations = self.get_missing_locations(states, locations).await?;

        if !wait {
            let not_ready_states_with_missing_locations = missing_locations
                .iter()
                .any(|(state, locations)| !state.is_ready() && !locations.is_empty());
            let multiple_missing_locations = missing_locations
                .iter()
                .any(|(_, locations)| locations.len() > 1);

            if not_ready_states_with_missing_locations || multiple_missing_locations {
                let add_locations = generate_add_location_commands(&missing_locations).join("\n");
                let text = format!(
                    r#"Some states will need multiple locations to be added
and it is not possible at the moment without '--wait' flag.

You can either re-run this command with '--wait' flag
or run following commands before running 'statehub register-cluster`

{}
"#,
                    add_locations
                );
                anyhow::bail!(text)
            }
        }

        for (state, locations) in &missing_locations {
            self.add_missing_locations(state, locations, wait).await?;
        }

        Ok(())
    }

    async fn get_missing_locations(
        &self,
        states: &[v0::StateName],
        locations: &[Location],
    ) -> anyhow::Result<IndexMap<v0::State, Vec<Location>>> {
        let mut missing_locations = IndexMap::new();
        for state in states {
            let state = self.api.get_state(state).await?;
            let missing = locations
                .iter()
                .filter(|location| !state.is_available_in(location))
                .copied()
                .collect();
            let state = state.into_inner();
            missing_locations.insert(state, missing);
        }
        Ok(missing_locations)
    }

    async fn add_missing_locations(
        &self,
        state: &v0::State,
        locations: &[Location],
        wait: bool,
    ) -> anyhow::Result<()> {
        for location in locations {
            if state.is_available_in(location) {
                log::info!(
                    "Skipping state {} which is already available in {}",
                    state.name,
                    location
                );
            } else {
                self.inform(format_args!(
                    "Extending state {} to {}",
                    state.name, location
                ));
                self.add_location_helper(state, location, wait).await?;
            }
        }

        Ok(())
    }

    pub(super) async fn setup_configmap_helper(
        &self,
        kubectl: &k8s::Kubectl,
        statehub_cluster: &StatehubCluster,
        default_state: &str,
        cleanup_grace: &str,
        // register_cluster: &RegisterCluster,
    ) -> anyhow::Result<()> {
        let operator = kubectl.get_operator().await?;
        let namespace = statehub_cluster.namespace();
        let api = self.api.url("");

        let event = self.setup_configmap_event(namespace);
        let event = operator
            .create_configmap(statehub_cluster, default_state, &api, cleanup_grace)
            .await
            .with_event(event);
        self.report_event(event).await?;

        Ok(())
    }

    pub(super) async fn setup_cluster_token_helper(
        &self,
        kubectl: &k8s::Kubectl,
        cluster: &v0::Cluster,
        statehub_cluster: &StatehubCluster,
    ) -> anyhow::Result<()> {
        let token = self.api.issue_cluster_token(&cluster.name).await?;
        self.verbosely(format!("Issued token {} for {}", token.token, cluster))?;
        let operator = kubectl.get_operator().await?;

        let event = self.store_cluster_token_event(statehub_cluster.namespace());
        let event = operator
            .create_cluster_token_secret(statehub_cluster, &token.token)
            .await
            .with_event(event);
        self.report_event(event).await?;

        Ok(())
    }

    pub(super) async fn setup_credentials_helper(
        &self,
        kubectl: &k8s::Kubectl,
        statehub_cluster: &StatehubCluster,
        aws: &AwsCredentials,
        azure: &AzureCredentials,
    ) -> anyhow::Result<()> {
        let aws = json::to_value(aws)?;
        let azure = json::to_value(azure)?;
        let operator = kubectl.get_operator().await?;

        operator
            .create_aws_credentials_secret(statehub_cluster, aws)
            .await?;
        operator
            .create_azure_credentials_secret(statehub_cluster, azure)
            .await?;

        Ok(())
    }

    pub(super) async fn claim_unowned_states_helper(
        &self,
        cluster: &v0::Cluster,
        register_cluster: &RegisterCluster,
    ) -> anyhow::Result<()> {
        if let Some(ref states) = register_cluster.states {
            for state in states {
                if self.api.get_state(state).await?.owner.is_none() {
                    self.verbosely(format!("Claiming ownership of state {}", state))?;
                    self.api.set_owner(state, &cluster.name).await?;
                }
            }
        }
        Ok(())
    }

    pub(super) async fn relinquish_states_helper(
        &self,
        cluster: &v0::ClusterName,
    ) -> anyhow::Result<()> {
        for state in self.api.get_all_states().await? {
            if state.owner.as_ref() == Some(cluster) {
                log::info!("Relinquish ownership for state {}", state.name);
                self.api.unset_owner(&state.name).await?;
            } else if let Some(owner) = state.owner {
                log::debug!("Skipping state {} (owned by {})", state.name, owner);
            } else {
                log::debug!("Skipping state {} (unowned)", state.name);
            }
        }
        Ok(())
    }

    pub(super) async fn delete_volume_helper(
        &self,
        state: &v0::StateName,
        volume: &v0::VolumeName,
        wait: bool,
    ) -> anyhow::Result<Output<v0::Volume>> {
        let mut volume = self.api.delete_volume(state, volume).await?;
        if wait {
            let delay = time::Duration::from_secs(5);
            loop {
                match self.api.get_volume(state, &volume.name).await {
                    Ok(deleting) => volume = deleting,
                    Err(err) if is_volume_not_found(&err) => break,
                    _ => continue,
                }
            }
            time::sleep(delay).await;
        }

        Ok(volume)
    }

    pub(super) fn login_prompt_helper(&self) -> anyhow::Result<(String, String)> {
        let username = whoami::username();
        let hostname = whoami::hostname();
        let login = v0::Login { username, hostname };
        let text = json::to_string(&login)?;
        let token = base64::encode(&text);
        let id = format!("{}@{}", login.username, login.hostname);
        Ok((token, id))
    }

    pub(super) async fn helm_install_helper(
        &self,
        kubectl: &k8s::Kubectl,
        cluster: &v0::Cluster,
        namespace: &str,
        helm: &k8s::Helm,
    ) -> anyhow::Result<()> {
        let kube_context = kubectl.context();
        let commands = helm.install_command(cluster, Some(kube_context), namespace);

        let event = self.helm_install_event(namespace, &commands);
        let event = helm.execute(commands).await.with_event(event);
        let (stdout, stderr) = self.report_event(event).await?;

        self.verbosely(stdout)?;
        self.error(stderr)?;
        Ok(())
    }

    pub(super) async fn helm_uninstall_helper(
        &self,
        kubectl: &k8s::Kubectl,
        cluster: &v0::Cluster,
        namespace: &str,
        helm: &k8s::Helm,
    ) -> anyhow::Result<()> {
        let kube_context = kubectl.context();
        let commands = helm.uninstall_command(cluster, Some(kube_context), namespace);

        let (stdout, stderr) = helm.execute(commands).await?;

        self.verbosely(stdout)?;
        self.error(stderr)?;
        Ok(())
    }

    pub(super) async fn check_for_update_helper(
        &self,
    ) -> Option<(semver::Version, semver::Version)> {
        let current = Self::VERSION.parse::<semver::Version>().ok()?;
        let latest = self.latest_version().await.ok()?;

        log::debug!(
            "Check for update - current: {}, latest: {}",
            current,
            latest
        );

        if current < latest {
            Some((current, latest))
        } else {
            None
        }
    }

    pub(super) async fn update_helper(&self, version: semver::Version) -> anyhow::Result<()> {
        let version = version.to_string();
        self.inform(format_args!("Downloading version {}", version));
        let file = self
            .get_package("statehub", &version)
            .await
            .context(format!("Downloading version {}", version))?;
        log::debug!("Downloaded {} bytes", file.len());
        self.replace_statehub_binary(file)
            .ok_or_else(|| anyhow::anyhow!("Failed to replace the statehub binary"))
    }

    fn replace_statehub_binary(&self, contents: bytes::Bytes) -> Option<()> {
        let statehub_exe = env::current_exe().ok()?;
        let permissions = fs::metadata(&statehub_exe).ok()?.permissions();
        let download = statehub_exe.parent()?.join(".statehub-download");
        fs::write(&download, contents).ok()?;
        fs::remove_file(&statehub_exe).ok()?;
        fs::rename(&download, &statehub_exe).ok()?;
        fs::set_permissions(statehub_exe, permissions).ok()
    }
}

fn is_volume_not_found(err: &anyhow::Error) -> bool {
    err.downcast_ref::<v0::Error>()
        .map(v0::Error::is_volume_not_found)
        .unwrap_or_default()
}

#[derive(Clone, Debug)]
pub(super) enum AddLocation {
    FromLocation(Location),
    FromCluster(v0::ClusterName),
}

fn generate_add_location_commands(
    missing_locations: &IndexMap<v0::State, Vec<Location>>,
) -> Vec<String> {
    missing_locations
        .iter()
        .flat_map(|(state, locations)| {
            locations.iter().map(move |location| {
                format!("statehub add-location --wait {} {}", state.name, location)
            })
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    fn state(name: impl AsRef<str>) -> v0::State {
        let name = v0::StateName::from(name.as_ref());
        v0::State {
            id: uuid::Uuid::default(),
            name,
            created: chrono::Utc::now(),
            modified: chrono::Utc::now(),
            storage_class: None,
            locations: v0::StateLocations::default(),
            owner: None,
            provisioning_status: v0::ProvisioningStatus::default(),
            allowed_clusters: None,
            condition: v0::Condition::Green,
        }
    }

    #[test]
    fn generate_add_location_commands() {
        let missing_locations = vec![
            (state("alfa"), vec![Location::Aws(AwsRegion::UsWest2)]),
            (
                state("bravo"),
                vec![
                    Location::Aws(AwsRegion::UsEast1),
                    Location::Azure(AzureRegion::EastUs2),
                ],
            ),
            (state("charlie"), vec![Location::Aws(AwsRegion::UsWest2)]),
        ]
        .into_iter()
        .collect();
        let cmds = super::generate_add_location_commands(&missing_locations);
        assert_eq!(cmds[0], "statehub add-location --wait alfa aws/us-west-2");
        assert_eq!(cmds[1], "statehub add-location --wait bravo aws/us-east-1");
        assert_eq!(cmds[2], "statehub add-location --wait bravo azure/eastus2");
        assert_eq!(
            cmds[3],
            "statehub add-location --wait charlie aws/us-west-2"
        );
    }
}
