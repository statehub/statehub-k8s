//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use clap::Parser;
use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;

#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Parser)]
#[serde(rename_all = "UPPERCASE")]
pub(crate) struct AzureCredentials {
    // #[clap(help = "Azure Tenant ID", long, requires_all = &["azure_client_id", "azure_client_secret"])]
    #[clap(help = "Azure Tenant ID", long)]
    azure_tenant_id: Option<String>,
    // #[clap(help = "Azure Client ID", long, requires_all = &["azure_tenant_id", "azure_client_secret"])]
    #[clap(help = "Azure Client ID", long)]
    azure_client_id: Option<String>,
    // #[clap(help = "Azure Client Secret", long, requires_all = &["azure_tenant_id", "azure_client_id"])]
    #[clap(help = "Azure Client Secret", long)]
    azure_client_secret: Option<String>,
}

impl AzureCredentials {
    pub(super) fn validate(&self) -> anyhow::Result<()> {
        let all = self.azure_tenant_id.is_some()
            && self.azure_client_id.is_some()
            && self.azure_client_secret.is_some();

        let nothing = self.azure_tenant_id.is_none()
            && self.azure_client_id.is_none()
            && self.azure_client_secret.is_none();

        if all || nothing {
            Ok(())
        } else {
            Err(anyhow::anyhow!(
                "Either all of '{}', '{}' and '{}' options must be set or none of them",
                "azure-tenant-id",
                "azure-client-id",
                "azure-client-secret"
            ))
        }
    }

    pub(super) fn missing(&self) -> bool {
        self.azure_tenant_id.is_none()
            || self.azure_client_id.is_none()
            || self.azure_client_secret.is_none()
    }

    pub(super) fn new(tenant_id: String, client_id: String, client_secret: String) -> Self {
        let azure_tenant_id = Some(tenant_id);
        let azure_client_id = Some(client_id);
        let azure_client_secret = Some(client_secret);

        Self {
            azure_tenant_id,
            azure_client_id,
            azure_client_secret,
        }
    }
}

#[cfg(test)]
mod tests {
    use serde_json as json;

    use super::*;

    #[test]
    fn deserialize() {
        let azure = AzureCredentials {
            azure_tenant_id: String::new().into(),
            azure_client_id: String::new().into(),
            azure_client_secret: String::new().into(),
        };

        let azure = json::to_value(azure).unwrap();
        assert!(azure.is_object());
        assert!(azure.get("AZURE_TENANT_ID").is_some());
        assert!(azure.get("AZURE_CLIENT_ID").is_some());
        assert!(azure.get("AZURE_CLIENT_SECRET").is_some());
        assert!(azure.get("azure_client_secret").is_none());
    }

    #[test]
    fn azure_to_secret() {
        let azure = AzureCredentials {
            azure_tenant_id: String::from("tenant").into(),
            azure_client_id: String::from("client").into(),
            azure_client_secret: String::from("secret").into(),
        };

        let creds = json::to_value(azure).unwrap();
        let mut creds = creds
            .as_object()
            .into_iter()
            .flatten()
            .map(|(key, value)| (key.to_string(), value.as_str().unwrap_or_default()));

        let (key, value) = creds.next().unwrap();
        assert_eq!(key, "AZURE_TENANT_ID");
        assert_eq!(value, "tenant");

        let (key, value) = creds.next().unwrap();
        assert_eq!(key, "AZURE_CLIENT_ID");
        assert_eq!(value, "client");

        let (key, value) = creds.next().unwrap();
        assert_eq!(key, "AZURE_CLIENT_SECRET");
        assert_eq!(value, "secret");

        assert!(creds.next().is_none());
    }
}
