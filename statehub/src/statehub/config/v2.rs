//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub(crate) struct Config {
    version: String,
    api: String,
    console: String,
    token: Option<String>,
}

impl Config {
    const VERSION: &'static str = "2";

    pub(super) fn validate_config(config: &toml::Value) -> anyhow::Result<Self> {
        let version = config.get("version").and_then(|version| version.as_str());
        anyhow::ensure!(version == Some(Self::VERSION), "Unknown version");
        let config = config.clone();
        toml::Value::try_into(config).context("Converting to ConfigV2")
    }

    pub(crate) fn api(&self) -> &str {
        &self.api
    }

    pub(crate) fn token(&self) -> Option<&str> {
        self.token.as_deref()
    }
}

impl From<super::ConfigV1> for Config {
    fn from(v1: super::ConfigV1) -> Self {
        let version = Self::VERSION.to_string();
        let api = v1.api().to_string();
        let console = DEFAULT_CONSOLE.to_string();
        let token = v1.token().map(ToString::to_string);
        Self {
            version,
            api,
            console,
            token,
        }
    }
}
