//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use serde_json as json;

use super::*;

#[test]
fn duration() {
    let event = StatehubEvent::new("foo", "bar");
    // println!("{}", json::to_string_pretty(&event).unwrap());
    let value = json::to_value(&event).unwrap();
    let duration = value["duration"].to_string();
    // println!("{}", duration);
    assert!(duration.starts_with(r#""PT0."#));
}
