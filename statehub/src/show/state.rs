//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

const OK: Emoji<'static, 'static> = Emoji("🆗", "[v]");
// const PROVISIONING: Emoji<'static, 'static> = Emoji("⤴ 🔜", "[+]");
const PROVISIONING: Emoji<'static, 'static> = Emoji("⤴\u{fe0f}", "[+]");
const RECOVERING: Emoji<'static, 'static> = Emoji("🔄", "[~]");
const DELETING: Emoji<'static, 'static> = Emoji("⤵\u{fe0f}", "[-]");
const ERROR: Emoji<'static, 'static> = Emoji("❌", "[x]");

const OWNED: Emoji<'static, 'static> = Emoji("🔒 ", "");
const UNOWNED: Emoji<'static, 'static> = Emoji("🔓", "-");

const GREEN: Emoji<'static, 'static> = Emoji("🟢", "[GREEN] ");
const YELLOW: Emoji<'static, 'static> = Emoji("🟡", "[YELLOW]");
const RED: Emoji<'static, 'static> = Emoji("🔴", "[RED]   ");

fn show_owner(owner: Option<&str>) -> String {
    owner.map_or_else(
        || format!("{}", UNOWNED),
        |cluster| format!("{}{}", OWNED, cluster),
    )
}

impl Show for v0::State {
    fn show(&self) -> String {
        let volumes = match self.count_volumes() {
            0 => String::from("(no volumes)"),
            1 => String::from("(1 volume)"),
            n => format!("({} volumes)", n),
        };

        let owner = show_owner(self.owner.as_deref());

        format!(
            "{} {:>24} {} {:<12} [{:#}] ({})",
            get_label(&self.name),
            self.name,
            self.condition.show(),
            volumes,
            self.locations.show(),
            owner,
        )
    }

    fn detailed_show(&self) -> String {
        let volumes = self
            .collect_volumes()
            .into_iter()
            .map(|(name, locations)| {
                format!(
                    "  {}:\n    {}",
                    name,
                    locations
                        .iter()
                        .map(|(location, volume)| format!(
                            "{:#}: {}",
                            location,
                            volume.status.value.show()
                        ))
                        .join(", ")
                )
            })
            .join("\n");

        let storage_class = self
            .storage_class
            .as_ref()
            .map(|sc| format!("{} ({})", sc.name, sc.fs_type))
            .unwrap_or_default();
        let owner = show_owner(self.owner.as_deref());
        format!(
            "{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}",
            format_args!("State:         {}", self.name),
            format_args!("Id:            {}", self.id),
            format_args!("Storage Class: {}", storage_class),
            format_args!("Owner:         {}", owner),
            format_args!("Created:       {}", HumanTime::from(self.created)),
            format_args!("Modified:      {}", HumanTime::from(self.modified)),
            format_args!("Condition:     {}", self.condition.show()),
            format_args!("Locations:\n{}", self.locations.detailed_show()),
            format_args!("Volumes:\n{}", volumes)
        )
    }
}

impl Show for Vec<v0::State> {
    fn show(&self) -> String {
        self.iter().map(Show::show).join("\n")
    }
}

impl Show for v0::StateLocations {
    fn show(&self) -> String {
        let aws = self
            .aws
            .iter()
            .map(|location| format!("{:#} {}", location.region, location.status.show()));
        let azure = self
            .azure
            .iter()
            .map(|location| format!("{:#} {}", location.region, location.status.show()));
        aws.chain(azure).join(", ")
    }

    fn detailed_show(&self) -> String {
        let none = || String::from("None");
        let aws = self.aws.iter().map(|location| {
            format!(
                " {:#}:\n  {}\n  {}",
                location.region,
                format_args!("Status: {}", location.status.show()),
                format_args!(
                    "PLS   : {}",
                    location
                        .private_link_service
                        .as_ref()
                        .map_or_else(none, |pls| pls.detailed_show())
                ),
            )
        });
        let azure = self.azure.iter().map(|location| {
            format!(
                " {:#}:\n  {}\n  {}",
                location.region,
                format_args!("Status: {}", location.status.show()),
                format_args!(
                    "PLS   : {}",
                    location
                        .private_link_service
                        .as_ref()
                        .map_or_else(none, |pls| pls.detailed_show())
                )
            )
        });

        aws.chain(azure).join("\n")
    }
}

impl Show for v0::StateLocationStatus {
    fn show(&self) -> String {
        let text = match self {
            Self::Ok => OK,
            Self::Provisioning => PROVISIONING,
            Self::Recovering => RECOVERING,
            Self::Deleting => DELETING,
            Self::Error => ERROR,
        };
        text.to_string()
    }
}

impl Show for v0::PrivateLinkServiceAws {
    fn show(&self) -> String {
        format!("{} / {}", self.id, self.name)
    }
}

impl Show for v0::PrivateLinkServiceAzure {
    fn show(&self) -> String {
        self.id.clone()
    }
}

impl Show for v0::Condition {
    fn show(&self) -> String {
        let condition = match self {
            Self::Green => GREEN,
            Self::Yellow => YELLOW,
            Self::Red => RED,
        };
        format!("{}", condition)
    }
}

impl Show for v0::CreateStateDto {
    fn show(&self) -> String {
        let aws = self.locations.aws.iter().map(|dto| dto.region.into());
        let azure = self.locations.azure.iter().map(|dto| dto.region.into());
        let locations = aws.chain(azure).collect::<Vec<Location>>();

        let locations = if locations.is_empty() {
            String::new()
        } else {
            format!(" in {}", locations.show())
        };
        format!("Creating new state '{}'{}", self.name, locations)
    }
}
