//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

const GITLAB_STATEHUB_GENERIC_PACKAGES: &str = "/projects/25498479/packages/generic";

#[async_trait::async_trait]
pub(crate) trait GitlabGenericPackage {
    const BASE_URL: &'static str;
    const API_VERSION: &'static str;
    async fn get_file(
        &self,
        package: &str,
        version: &str,
        file: &str,
    ) -> anyhow::Result<bytes::Bytes>;

    fn gitlab() -> Self;
}

#[async_trait::async_trait]
impl GitlabGenericPackage for Api {
    const BASE_URL: &'static str = "https://gitlab.com";
    const API_VERSION: &'static str = "/api/v4";

    async fn get_file(
        &self,
        package: &str,
        version: &str,
        file: &str,
    ) -> anyhow::Result<bytes::Bytes> {
        let path = format!(
            "{}/{}/{}/{}",
            GITLAB_STATEHUB_GENERIC_PACKAGES, package, version, file,
        );

        self.get_impl(path).await
    }

    fn gitlab() -> Self {
        Self::new(Self::BASE_URL, Self::API_VERSION, None)
            .connect_timeout(time::Duration::from_secs(1))
    }
}
