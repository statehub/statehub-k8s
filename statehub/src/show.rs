//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::collections::HashMap;
use std::iter;
use std::process::Command;

use chrono_humanize::HumanTime;
use console::Emoji;
use itertools::{join, Itertools};

use crate::k8s;
use crate::location::Location;
use crate::traits::Show;
use crate::v0;

pub(crate) use detailed::Detailed;
pub(crate) use fun::get_label;
pub(crate) use quiet::Quiet;

mod cluster;
mod detailed;
mod fun;
mod location;
mod quiet;
mod state;
mod volume;

impl Show for String {
    fn show(&self) -> String {
        self.to_string()
    }
}

impl Show for () {
    fn show(&self) -> String {
        String::new()
    }
}

impl Show for HashMap<String, Vec<String>> {
    fn show(&self) -> String {
        let mut out = String::new();

        for (region, nodes) in self {
            let region = if region.is_empty() {
                "No region"
            } else {
                region.as_str()
            };
            out += &format!("{}:    {}\n", region, join(nodes, ", "));
        }
        out
    }
}

impl Show for Command {
    fn show(&self) -> String {
        let cmd = iter::once(self.get_program())
            .chain(self.get_args())
            .map(|elt| elt.to_string_lossy())
            .format(" ");
        format!("{}", cmd)
    }

    fn detailed_show(&self) -> String {
        format!("             {}", self.show())
    }
}

impl Show for Vec<Command> {
    fn show(&self) -> String {
        self.iter().map(Show::show).join("\n")
    }

    fn detailed_show(&self) -> String {
        self.iter().map(Show::detailed_show).join("\n")
    }
}
