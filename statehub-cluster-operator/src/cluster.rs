//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::fmt;

use kube::Resource as _;
use kube::ResourceExt as _;
use statehub_k8s_helper as k8s;

use k8s::appsv1;
use k8s::corev1;
use k8s::ConfigMapExt;
use k8s::NamespaceExt;
use k8s::ResourceExt;
use k8s::SecretExt;

pub use check::CheckObject;

use super::StatehubCluster;

mod check;
mod cloud_credentials;
mod cluster_token;
mod configmap;
mod daemonset;
mod deployment;
mod helper;

pub const STATEHUB_CLUSTER_CONFIGMAP_NAME: &str = "statehub";

pub struct Operator {
    kubectl: k8s::Kubectl,
    // If I am controller this is my own deployment object
    controller: Option<appsv1::Deployment>,
}

impl Operator {
    pub async fn new() -> kube::Result<Self> {
        k8s::Kubectl::try_default().await.map(Self::from_kubectl)
    }

    pub fn with_config(config: kube::Config) -> kube::Result<Self> {
        k8s::Kubectl::from_config(config).map(Self::from_kubectl)
    }

    fn from_kubectl(kubectl: k8s::Kubectl) -> Self {
        Self {
            kubectl,
            controller: None,
        }
    }

    pub async fn create_namespace(
        &self,
        cluster: &StatehubCluster,
    ) -> kube::Result<corev1::Namespace> {
        let owner = self
            .controller
            .as_ref()
            .map(|deployment| deployment.object_ref(&()))
            .and_then(|owner| k8s::owner_reference(owner, true, true));
        let namespace = if let Some(owner) = owner {
            corev1::Namespace::new(cluster.namespace()).owner(owner)
        } else {
            corev1::Namespace::new(cluster.namespace())
        };
        self.kubectl
            .ensure_global_k_is_installed_forced(namespace)
            .await
    }

    pub async fn delete_namespace(&self, statehub: &StatehubCluster) -> kube::Result<()> {
        let dp = self.kubectl.delete_params();
        let namespaces = self.kubectl.namespaces();
        kube::runtime::wait::delete::delete_and_finalize(namespaces, statehub.namespace(), &dp)
            .await
            .map_err(|err| match err {
                kube::runtime::wait::delete::Error::NoUid => todo!(),
                kube::runtime::wait::delete::Error::Delete(err) => err,
                kube::runtime::wait::delete::Error::Await(_) => todo!(),
            })
    }

    pub async fn check_namespace(
        &self,
        cluster: &StatehubCluster,
    ) -> kube::Result<Option<corev1::Namespace>> {
        let lp = self.kubectl.list_params();
        let namespace = self
            .kubectl
            .namespaces()
            .list(&lp)
            .await?
            .items
            .into_iter()
            .find(|namespace| namespace.name() == cluster.namespace());
        Ok(namespace)
    }

    pub async fn ensure_namespace_exists(
        &self,
        cluster: &StatehubCluster,
    ) -> kube::Result<corev1::Namespace> {
        if let Ok(Some(namespace)) = self.check_namespace(cluster).await {
            Ok(namespace)
        } else {
            self.create_namespace(cluster).await
        }
    }
}

impl fmt::Debug for Operator {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Kubectl")
            .field("client", &"kube::Client")
            .finish()
    }
}
