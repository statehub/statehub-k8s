//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

const STATEHUB_CLUSTER_TOKEN_SECRET_TYPE: &str = "statehub.io/cluster-token";
const STATEHUB_CLUSTER_TOKEN_SECRET_NAME: &str = "statehub-cluster-token";

impl Operator {
    pub async fn create_cluster_token_secret(
        &self,
        cluster: &StatehubCluster,
        token: &str,
    ) -> kube::Result<corev1::Secret> {
        let secret = corev1::Secret::new(STATEHUB_CLUSTER_TOKEN_SECRET_NAME)
            .r#type(STATEHUB_CLUSTER_TOKEN_SECRET_TYPE)
            .string_data([("cluster-token", token)]);

        self.kubectl
            .ensure_namespaced_k_is_installed_forced(secret, cluster.namespace())
            .await
    }

    pub async fn delete_cluster_token_secret(&self, cluster: &StatehubCluster) -> kube::Result<()> {
        let dp = self.kubectl.delete_params();
        let secrets = self.kubectl.secrets(cluster.namespace());
        kube::runtime::wait::delete::delete_and_finalize(
            secrets,
            STATEHUB_CLUSTER_TOKEN_SECRET_NAME,
            &dp,
        )
        .await
        .map_err(|err| match err {
            kube::runtime::wait::delete::Error::NoUid => todo!(),
            kube::runtime::wait::delete::Error::Delete(err) => err,
            kube::runtime::wait::delete::Error::Await(_) => todo!(),
        })
    }

    pub async fn check_cluster_token_secret(
        &self,
        cluster: &StatehubCluster,
    ) -> kube::Result<CheckObject<corev1::Secret>> {
        self.check_namespaced_object(cluster.namespace(), STATEHUB_CLUSTER_TOKEN_SECRET_NAME)
            .await
    }
}
