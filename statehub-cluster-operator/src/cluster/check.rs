//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use serde::de;

use super::*;

impl Operator {
    pub(crate) async fn check_namespaced_object<K>(
        &self,
        namespace: &str,
        name: &str,
    ) -> kube::Result<CheckObject<K>>
    where
        K: kube::Resource,
        <K as kube::Resource>::DynamicType: Default,
        K: Clone + de::DeserializeOwned + fmt::Debug,
    {
        let object = self.find_namespaced_resource(namespace, name).await?;

        Ok(CheckObject::from_option(object, name))
    }
}

#[derive(Debug)]
pub enum CheckObject<T> {
    Found(T),
    NotFound(String),
}

impl<T> CheckObject<T>
where
    T: kube::ResourceExt,
{
    pub(crate) fn from_option(object: Option<T>, name: &str) -> Self {
        match object {
            Some(object) => Self::Found(object),
            None => Self::NotFound(name.to_string()),
        }
    }

    pub fn found(&self) -> bool {
        match self {
            Self::Found(_) => true,
            Self::NotFound(_) => false,
        }
    }

    pub fn name(self) -> String {
        match self {
            Self::Found(object) => object.name(),
            Self::NotFound(name) => name,
        }
    }
}
