//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use kube::CustomResource;
use kube::CustomResourceExt;
use statehub_k8s_helper as k8s;
// use kube::ResourceExt;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use k8s::apiextensionsv1;

#[derive(Clone, Debug, Serialize, Deserialize, CustomResource, JsonSchema)]
#[kube(
    kind = "StatehubCluster",
    group = "statehub.io",
    version = "v1alpha1",
    shortname = "shc"
)]
pub struct ClusterSpec {
    pub namespace: String,
}

impl StatehubCluster {
    pub fn with_namespace(name: &str, namespace: &str) -> Self {
        let namespace = namespace.to_string();
        let spec = ClusterSpec { namespace };
        Self::new(name, spec)
    }

    pub fn singleton(_name: &str) -> apiextensionsv1::CustomResourceDefinition {
        // let crd = Self::crd();
        // let mut version = crd.spec;
        Self::crd()
    }

    pub fn namespace(&self) -> &str {
        &self.spec.namespace
    }
}
